<?php

namespace MSR\Parser;

class SrtParser implements Parser
{
    /**
     * @throws WrongFormat
     */
    public function countWordsOccurrence(string $content): array
    {
        $this->checkFormat($content);

        $lines = $this->extractTextLines($content);

        $wordsAccumulator = [];
        foreach ($lines as $line) {
            $words = [];
            preg_match_all('/[a-zA-Z]+/', $line, $words);
            foreach ($words[0] as $word) {
                $wordsAccumulator[$word] = isset($wordsAccumulator[$word]) ? $wordsAccumulator[$word]+1 : 1;
            }
        }

        return $wordsAccumulator;
    }

    private function checkFormat(string $content)
    {
        if (empty($content)) {
            throw new WrongFormat('Empty input.');
        }

        if ($content{0} != 1) {
            throw new WrongFormat(sprintf('Bad starting character, should start with `1` not with `%s`.', $content{0}));
        }
    }

    private function extractTextLines(string $content): array
    {
        $lines = [];
        foreach (explode("\n", $content) as $line) {
            if (empty($line)) {
                continue;
            }
            if (preg_match('/^[0-9]{1,10}$/', $line)) {
                continue;
            }
            if (preg_match('/^[0-9:,]{12} --> [0-9:,]{12}$/', $line)) { // <-- pattern could be more accurate ;)
                continue;
            }

            $lines[] = strip_tags($line);
        }

        return $lines;
    }
}
