<?php

namespace MSR\Parser;

class AnyParser implements Parser
{
    public function countWordsOccurrence(string $content): array
    {
        $wordsAccumulator = [];

        preg_match_all('/[a-zA-Z]+/', $content, $words);
        foreach ($words[0] as $word) {
            $wordsAccumulator[$word] = isset($wordsAccumulator[$word]) ? $wordsAccumulator[$word]+1 : 1;
        }

        return $wordsAccumulator;
    }
}
