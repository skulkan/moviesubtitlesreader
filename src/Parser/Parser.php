<?php

namespace MSR\Parser;

interface Parser
{
    public function countWordsOccurrence(string $content): array;
}