<?php

namespace MSR;

use MSR\Parser\AnyParser;
use MSR\Parser\SrtParser;

class ParsingServiceProvider
{
    public function provide()
    {
        $service = new ParsingService();
        $service->registerParser(new SrtParser());
        $service->registerParser(new AnyParser());

        return $service;
    }
}
