<?php

namespace MSR;

use MSR\Parser\Parser;
use MSR\Parser\WrongFormat;

class ParsingService
{
    /** @var Parser[] */
    private $parsers = [];

    public function registerParser(Parser $parser)
    {
        $this->parsers[] = $parser;
    }

    public function countWordsOccurrenceForFile(string $filePath)
    {
        if (($content = file_get_contents($filePath)) === false) {
            throw new FileNotFound();
        }

        $output = null;
        foreach ($this->parsers as $parser) {
            try {
                $output = $parser->countWordsOccurrence($content);
                break;
            } catch (WrongFormat $e) {
                continue;
            }
        }

        if ($output === null) {
            throw new NoProperParser('Can\'t found proper parser.');
        }

        arsort($output);

        return $output;
    }
}
