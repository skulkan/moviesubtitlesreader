<?php

use MSR\NoProperParser;
use PHPUnit\Framework\TestCase;

class ParsingServiceTest extends TestCase
{
    public function testWithoutNotRestrictedParser()
    {
        $this->expectException(NoProperParser::class);

        $service = new \MSR\ParsingService();
        $service->registerParser(new \MSR\Parser\SrtParser());

        $filePath = __DIR__ . '/../files/Example.Short.3.txt';

        $service->countWordsOccurrenceForFile($filePath);
    }

    public function testWithProperRestrictedParser()
    {
        $service = new \MSR\ParsingService();
        $service->registerParser(new \MSR\Parser\SrtParser());

        $filePath = __DIR__ . '/../files/Example.Short.3.srt';

        $output = $service->countWordsOccurrenceForFile($filePath);

        $this->assertEquals(12, count($output));
        $this->assertEquals(4, current($output));
        $this->assertEquals('linia', key($output));

    }

    public function testWithNotRestrictedParser()
    {
        $service = new \MSR\ParsingService();
        $service->registerParser(new \MSR\Parser\AnyParser());

        $filePath = __DIR__ . '/../files/Example.Short.3.srt';

        $output = $service->countWordsOccurrenceForFile($filePath);

        // Usage of not restricted parser gives wrong response, SRT tags are count as normal words
        $this->assertEquals(13, count($output));
        $this->assertEquals(4, current($output));
        $this->assertEquals('b', key($output));
    }
}
