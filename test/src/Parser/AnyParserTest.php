<?php

use PHPUnit\Framework\TestCase;

class AnyParserTest extends TestCase
{
    public function testSimpleInput()
    {
        $content = <<< FILE
1
00:00:11,020 --> 00:00:14,943
alfa, beta, tango

2
00:00:23,646 --> 00:00:28,258
alfa; alfa

3
00:00:29,149 --> 00:00:33,028
beta
FILE;

        $parser = new \MSR\Parser\AnyParser();
        $output = $parser->countWordsOccurrence($content);

        $this->assertEquals(3, $output['alfa']);
        $this->assertEquals(2, $output['beta']);
        $this->assertEquals(1, $output['tango']);
    }
}
