<?php

use PHPUnit\Framework\TestCase;

class SrtParserTest extends TestCase
{
    public function testEmptyInput()
    {
        $this->expectException(\MSR\Parser\WrongFormat::class);

        $content = '';

        $parser = new \MSR\Parser\SrtParser();
        $parser->countWordsOccurrence($content);
    }

    public function testSimpleInput()
    {
        $content = <<< FILE
1
00:00:11,020 --> 00:00:14,943
alfa, beta, tango

2
00:00:23,646 --> 00:00:28,258
alfa; alfa

3
00:00:29,149 --> 00:00:33,028
beta
FILE;

        $parser = new \MSR\Parser\SrtParser();
        $output = $parser->countWordsOccurrence($content);

        $this->assertEquals(3, $output['alfa']);
        $this->assertEquals(2, $output['beta']);
        $this->assertEquals(1, $output['tango']);
    }

    public function testSimpleInputWithTags()
    {
        $content = <<< FILE
1
00:00:11,020 --> 00:00:14,943
<i>alfa, <b>beta</b>, tango</i>

2
00:00:23,646 --> 00:00:28,258
alfa; alfa

3
00:00:29,149 --> 00:00:33,028
<strong>beta</strong>
FILE;

        $parser = new \MSR\Parser\SrtParser();
        $output = $parser->countWordsOccurrence($content);

        $this->assertEquals(3, $output['alfa']);
        $this->assertEquals(2, $output['beta']);
        $this->assertEquals(1, $output['tango']);
        $this->assertEquals(3, count($output));// <-- to be sure there is no more words
    }
}
