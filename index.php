<!DOCTYPE html>
<html>
<body>

<form action="/" method="post" enctype="multipart/form-data">
    Select file with movie subtitlies to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload File" name="submit">
</form>

</body>
</html>

<?php if (!empty($_FILES)) {
    require_once __DIR__ . '/vendor/autoload.php';
    $service = (new \MSR\ParsingServiceProvider())->provide();
    $output = $service->countWordsOccurrenceForFile($_FILES['fileToUpload']['tmp_name']);
    ?>

    <table>
        <tr>
            <th>Word</th>
            <th>Occurences</th>
        </tr>
        <?php foreach ($output as $word => $occurrences) : ?>
            <tr>
                <td><?=$word?></td>
                <td><?=$occurrences?></td>
            </tr>
        <?php endforeach; ?>

    </table>

<?php } ?>
