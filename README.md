# MovieSubtitlesReader

Problem description (polish)

Napisz skrypt PHP7, który parsuje pliki w formacie SRT oraz TXT (napisy dla filmów, jest wiele formatów). Wynikiem parsowania ma być tabela zawierająca tylko słowa występujące w napisach bez znaków specjalnych. Słowa mają być pogrupowane względem powtórzeń i przesortowane w kolejności od najczęściej powtarzanych. Pliki powinny być ładowane bezpośrednio przez przeglądarkę. Dla komentarzy i kodu użyj języka angielskiego.

How to run:
* checkout this repository and go into it
* "composer install"
* "php example/parse.php" to check from console
* "vendor/bin/phpunit" to run unit tests
* "php -S localhost:8000" and go to browser localhost:8000 to use http interface